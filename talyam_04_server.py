import socket 

PORT = 34012

OPTION_ONE = "you choose option 1!"
OPTION_TWO = "you choose option 2!"
OPTION_THREE = "you choose option 3!"
OPTION_FOUR = "you choose option 4!"
OPTION_FIVE = "you choose option 5!"
OPTION_SIX = "you choose option 6!"
OPTION_SEVEN = "you choose option 7!"
OPTION_EIGHT = "you choose option 8! bye!"

OPTION_NUMBER_INDEX = 6
BREAK = 8

def main():
    answers_dict = {1:OPTION_ONE, 2:OPTION_TWO, 3:OPTION_THREE, 4:OPTION_FOUR, 5:OPTION_FIVE, 6:OPTION_SIX, 7:OPTION_SEVEN, 8:OPTION_EIGHT}
    i = 0
    while True:
        with socket.socket() as listening_sock:
            #creating listeneing socket
            listening_sock.bind(('',PORT))
            listening_sock.listen(1)
            client_soc, client_address = listening_sock.accept()
            with client_soc:
                #creating socket with the client
                if i == 0:
                    #making sure welcome message sent only once
                    client_soc.sendall(b"Welcome to the pink floyd server!")
                    i += 1
                client_msg =  client_soc.recv(1024)
                client_msg = client_msg.decode()
                option = int(client_msg[OPTION_NUMBER_INDEX])
                msg = answers_dict.get(option)
                client_soc.sendall(msg.encode())
                if option == BREAK:
                    break



if __name__ == "__main__":
    main()