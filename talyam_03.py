import socket
import keyboard

PROXY_LISTEN_PORT = 9090
MOVIE_SERVER_IP = "54.71.128.194"
MOVIE_SERVER_PORT = 92

def main():
    while True:
        with socket.socket() as listening_sock:
            #creating listening socket 
            listening_sock.bind(('',PROXY_LISTEN_PORT))
            listening_sock.listen(1)
            client_soc, client_address = listening_sock.accept()
            with client_soc:
                #creating socket with the client 
                client_msg = client_soc.recv(1024)
                client_msg = client_msg.decode()
                print(client_msg)
                with socket.socket() as movie_server_socket:
                    #creating socket with the movie server 
                    server_address = (MOVIE_SERVER_IP, MOVIE_SERVER_PORT)
                    movie_server_socket.connect(server_address)
                    msg = client_msg
                    movie_server_socket.sendall(msg.encode())
                    server_msg = movie_server_socket.recv(1024)
                    server_msg = server_msg.decode() 
                    print(server_msg)
                    if "ERROR" not in server_msg:
                        start = 16
                        stop = server_msg.find("&directors")  
                        new_name = server_msg[start: stop - 1] + " proxy"
                        server_msg = server_msg[:start] + new_name + server_msg[stop - 1:]
                        server_msg = server_msg.replace("jpg", ".jpg") 
                    if ("france" in msg) or ("France" in msg):
                        server_msg = 'ERROR#"France is banned!"'
                client_soc.sendall(server_msg.encode())
                print(server_msg)
            
if __name__ == "__main__":
    main()