import socket 

SERVER_IP = "127.0.0.1"
SERVER_PORT = 34012

OPTION_ONE = '1'
OPTION_TWO = '2'
OPTION_THREE = '3'
OPTION_FOUR = '4'
OPTION_FIVE = '5'
OPTION_SIX = '6'
OPTION_SEVEN = '7'
EXIT = '8'

def print_menu():
    print('choose one of the options:\n1 - list of albums\n2 - list of songs in album\n3 - lenght of song')
    print('4 - lyrics of song\n5 - which album the song in?\n6 - search song by name\n7 - serach song by lyrics\n8 - quit')

def main():
    user_choice = '' 
    i = 0 
    options_dict = {OPTION_ONE:'option1;', 
        OPTION_TWO:'option2;user_input=""',
        OPTION_THREE:'option3;user_input=""',
        OPTION_FOUR:'option4;user_input=""',
        OPTION_FIVE:'option5;user_input=""',
        OPTION_SIX:'option6;user_input=""',
        OPTION_SEVEN:'option7;user_input=""',
        EXIT:'option8;'}
    while user_choice != '8':
        with socket.socket() as sock:
            #creating socket with the server
            server_address = (SERVER_IP, SERVER_PORT)
            sock.connect(server_address)
            if i == 0:
                #making sure welcome message deliver only once
                server_msg = sock.recv(1024)
                server_msg = server_msg.decode()
                print(server_msg)
                i += 1
            print_menu()
            try:
                user_choice = input()
                while user_choice > EXIT or user_choice < OPTION_ONE:
                    user_choice = input('invalid choice, choose one of the options, 1 - 8: ')
            except KeyboardInterrupt:
                break
            msg = options_dict.get(user_choice)
            sock.sendall(msg.encode())
            server_msg = sock.recv(1024)
            server_msg = server_msg.decode()
            print(server_msg)

if __name__ == "__main__":
    main()